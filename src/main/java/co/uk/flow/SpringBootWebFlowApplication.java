package co.uk.flow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootWebFlowApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootWebFlowApplication.class, args);
	}
}
